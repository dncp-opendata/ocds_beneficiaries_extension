# Extensión de Beneficiarios

Agrega una lista de `beneficiaries` a `Item`, para indicar los beneficiarios de los ítems a adquirir en una licitación.

## Paraguay

En Paraguay, algunos municipios y otras entidades públicas adquieren ítems para inversión en infraestructura educativa, utilizando un fondo especial llamado [FONACIDE](https://www.bacn.gov.py/leyes-paraguayas/3151/ley-n-4758-crea-el-fondo-nacional-de-inversion-publica-y-desarrollo-fonacide-y-el-fondo-para-la-excelencia-de-la-educacion-y-la-investigacion). 

Para estas licitaciones se tiene la información de los [establecimientos educativos](https://datos.mec.gov.py/def/establecimientos) que son beneficiados con esta licitación, así como la información de las [instituciones educativas](https://datos.mec.gov.py/def/instituciones) que son parte de cada establecimiento. 

Para modelar esta información en OCDS:

- Cada **establecimiento educativo** representa un `Beneficiary`
- Para cada `Beneficiary` se publica:
  - Su código de establecimiento en `beneficiaries/identifier/id`
  - El esquema del cuál proviene el código del establecimiento, en este caso el Ministerio de Educación (MEC), en `beneficiaries/identifier/scheme`
  - Su URI, referenciando a los datos abiertos del MEC, en `beneficiaries/identifier/uri`
  - Las **instituciones educativas** que funcionan en este establecimiento y **que serán beneficiadas por la inversión realizada**, en `beneficiaries/additionalIdentifiers`:
    - Su código de institución en `beneficiaries/additionalIdentifiers/id`
    - El esquema del cuál proviene el código de la institución, en este caso el Ministerio de Educación (MEC), en `beneficiaries/additionalIdentifiers/scheme`
    - Su URI, referenciando a los datos abiertos del MEC, en `beneficiaries/additionalIdentifiers/uri`
  - El monto y moneda destinado a este establecimiento en `beneficiaries/benefit`
  - El área de inversión (sanitarios, mobiliarios, aulas, otros), en `beneficiaries/classification`

## Ejemplo

```json
{
  "items": {
    "id": "1",
    "description": "Instalaciones sanitarias",
    "quantity": 1,
    "unit": {
      "name": "Unidad Medida Global",
      "id": "GL",
      "value": {
        "amount": 75000000,
        "currency": "PYG"
      }
    },
    "beneficiaries": [
      {
        "id": "PY-MEC-ESTABLECIMIENTOS-EDUCATIVOS-1701006",
        "identifier": {
          "id": "1701006",
          "scheme": "PY-MEC-ESTABLECIMIENTOS-EDUCATIVOS",
          "uri": "https://datos.mec.gov.py/doc/establecimientos/1701006"
        },
        "additionalIdentifiers": [
          {
            "id": "3120",
            "scheme": "PY-MEC-INSTITUCIONES-EDUCATIVAS",
            "uri": "https://datos.mec.gov.py/doc/instituciones/3120"
          }
        ],
        "benefit": {
          "value": {
            "amount": 37500000,
            "currency": "PYG"
          }
        },
        "classification": {
            "scheme": "PY-MEC-FONACIDE-AREA-INVERSION",
            "description": "Infraestructura:sanitarios; Infraestructura:mobiliarios"
        }
      }
    ]
  }
}
```


## Issues

Report issues for this extension in the [ocds-extensions repository](https://github.com/open-contracting/ocds-extensions/issues), putting the extension's name in the issue's title.

## Changelog

This extension was originally discussed in <https://github.com/open-contracting/standard/issues/1388>.
